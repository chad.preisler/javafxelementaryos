module chad.preisler.javafxelementaryos {
    requires javafx.controls;
    requires javafx.fxml;

    opens chad.preisler.javafxelementaryos to javafx.fxml;
    exports chad.preisler.javafxelementaryos;
}