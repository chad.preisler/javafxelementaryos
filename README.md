# JavaFX example app built on elementaryOS using Apache Netbeans

This is an example application that I built on my elementaryOS Linux box. I built
it using Java 14, JavaFX 14, and used Apache Netbeans 11.3 as my IDE. The project uses Maven.

Here is the Java 14 specific version information.

```
openjdk version "14" 2020-03-17
OpenJDK Runtime Environment AdoptOpenJDK (build 14+36)
OpenJDK 64-Bit Server VM AdoptOpenJDK (build 14+36, mixed mode, sharing)
```

# Configuring Netbeans

The OpenJFX site has a good description on how to do this.

https://openjfx.io/openjfx-docs/#IDE-NetBeans

Follow the "Modular with Maven" instructions.

# To build and Run using JDK 14

To build using Java version 14. Follow these instructions.

https://openjfx.io/openjfx-docs/#install-javafx

Here is the link to the Maven JavaFX plugin.

https://github.com/openjfx/javafx-maven-plugin

But to run on elementaryOS use this.

```
# this is the command line to run the app on elementaryOS 
# Java or gtk3 (not sure which) has a bug. The main window is not drawn correctly and the top bar will be missing if you use gtk3
java -cp target/javafxElementaryOS-1.0-SNAPSHOT.jar:$CLASSPATH --module-path $PATH_TO_FX --add-modules javafx.controls,javafx.fxml -Djdk.gtk.version=2 chad.preisler.javafxelementaryos.App

```

The POM is setup to run with the gtk2 argument also. I've also added a debug execution
that will allow you debug in the Netbeans IDE. 

```
             <plugin>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-maven-plugin</artifactId>
                <version>0.0.4</version>
                <executions>
                    <execution>
                        <!-- Default configuration for running -->
                        <id>default-cli</id>
                        <configuration>
                            <mainClass>chad.preisler.javafxelementaryos.App</mainClass>
                            <options>
                                <option>-Djdk.gtk.version=2</option>                            
                            </options>
                        </configuration>
                    </execution>
                    <execution>
                        <!-- Configuration for debugging -->
                        <id>debug</id>
                        <configuration>
                            <options>
                                <option>-agentlib:jdwp=transport=dt_socket,server=n,address=${jpda.address}</option>
                                <option>-Djdk.gtk.version=2</option>                            
                            </options>
                            <mainClass>chad.preisler.javafxelementaryos.App</mainClass>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

```

In order to debug the project from the Netbeans IDE you need to go into the project's
Properties->Actions->Debug Project and set it up so that the nbactions.xml `debug` looks like
the following:

```
        <action>
            <actionName>debug</actionName>
            <packagings>
                <packaging>jar</packaging>
            </packagings>
            <goals>
                <goal>javafx:run@debug</goal>
            </goals>
            <properties>
                <jpda.listen>true</jpda.listen>
            </properties>
        </action>
```


# Building native apps:
Use graalvm to build native apps for each platform. More detailed instructions 
are here.

https://docs.gluonhq.com/client/
https://github.com/gluonhq/client-maven-plugin
https://github.com/gluonhq/client-samples

Here is what I need to do to get this building on elementaryOS:

First I needed to install the following native tools and libraries:

```
sudo apt -y install gcc
sudo apt -y install pkg-config
sudo apt -y install pkgconf

sudo apt -y install libasound2-dev # (for pkgConfig alsa)
sudo apt -y install libavcodec-dev # (for pkgConfig libavcodec)
sudo apt -y install libavformat-dev # (for pkgConfig libavformat)
sudo apt -y install libavutil-dev # (for pkgConfig libavutil)
sudo apt -y install libfreetype6-dev # (for pkgConfig freetype2)
sudo apt -y install libgl-dev # (for pkgConfig gl)
sudo apt -y install libglib2.0-dev # (for pkgConfig gmodule-no-export-2.0)
sudo apt -y install libglib2.0-dev # (for pkgConfig gthread-2.0)
sudo apt -y install libgtk-3-dev # (for pkgConfig gtk+-x11-3.0)
sudo apt -y install libpango1.0-dev # (for pkgConfig pangoft2)
sudo apt -y install libx11-dev # (for pkgConfig x11)
sudo apt -y install libxtst-dev # (for pkgConfig xtst)
sudo apt -y install zlib1g-dev # (for pkgConfig zlib)
```

Here are the commands to build and run.

```
# export environment variables
# my graalVM is installed in /usr/lib/jvm
export GRAALVM_HOME=/usr/lib/jvm/graalvm-svm-linux-20.1.0-ea+28
export JAVA_HOME=$GRAALVM_HOME
# build the app
mvn clean client:build
# run the app
mvn client:run
```

I have not figured out how to get the native linux client to use gtk2 instead
of gtk3. The window is not drawn correctly using gtk3.

Apparently you need to tell the graalVM what classes can use reflection. Turns out
all the controllers need to be listed in the POM. Here is my Maven plugin 
configuration. Notice the classe listed in the `reflectionList". There is more info
about graalVM and reflection [here](https://docs.gluonhq.com/client/#_config_files)
under section 3.1.5 Config Files.

```
            <plugin>
                <groupId>com.gluonhq</groupId>
                <artifactId>client-maven-plugin</artifactId>
                <version>0.1.24</version>
                <configuration>
                    <mainClass>chad.preisler.javafxelementaryos.App</mainClass>                   
                    <reflectionList>
                        <list>javafx.fxml.FXMLLoader</list>
                        <list>chad.preisler.javafxelementaryos.PrimaryController</list>
                        <list>chad.preisler.javafxelementaryos.SecondaryController</list>
                        <list>javafx.scene.control.Button</list>
                        <list>javafx.scene.control.Label</list>
                    </reflectionList>
                </configuration>
            </plugin>           
```

